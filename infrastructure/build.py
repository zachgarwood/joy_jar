from pathlib import Path

from pyinfra import host
from pyinfra.operations import git, pip, server


if host.fact.directory(host.data.project_root):
    git.config(
        name="Set git user.name",
        repo=host.data.project_root,
        key="user.name",
        value="Joy Jar build script",
    )
    git.config(
        name="Set git user.email",
        repo=host.data.project_root,
        key="user.email",
        value="login@joyjar.zachgarwood.com",
    )
git.repo(
    name="Download source",
    src="https://gitlab.com/zachgarwood/joy_jar.git",
    dest=host.data.project_root,
    branch="production",
    rebase=True,
)
install_python_version = server.shell(
    name="Install Python version",
    commands="cat .python-version | pyenv install --skip-existing --verbose",
    chdir=host.data.project_root,
)
server.shell(
    name="Set global Python version",
    commands="pyenv global `cat .python-version`",
    chdir=host.data.project_root,
)
server.shell(
    name="Check Python version",
    commands=["pyenv version", "python --version"],
    chdir=host.data.project_root,
)
if install_python_version.changed:
    server.shell(
        name="Rehash pyenv shims",
        commands="pyenv rehash",
    )
server.shell(
    name="Check pip version",
    commands="pip --version",
    chdir=host.data.project_root,
)
pip.packages(
    name="Install requirements",
    latest=True,
    requirements=str(Path(host.data.project_root) / "requirements_ops.txt"),
    pip="/home/joyjar/.pyenv/versions/3.8.2/bin/pip",  # XXX This shouldn't be hardcoded
    # https://docs.pyinfra.com/en/latest/examples/dynamic_execution_deploy.html
)
server.shell(
    name="Migrate database",
    commands="python manage.py migrate",
    chdir=host.data.project_root,
)
server.shell(
    name="Collect static assets",
    commands="python manage.py collectstatic --no-input",
    chdir=host.data.project_root,
)

server.crontab(
    name="Clear sessions every day",
    command=f"cd {host.data.project_root} && python manage.py clearsessions",
    special_time="@daily",
)
