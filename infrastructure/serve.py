from pathlib import Path

from pyinfra import host
from pyinfra.operations import files, init, server


if not host.fact.directory("/etc/letsencrypt/live/joyjar.zachgarwood.com"):
    server.shell(
        name="Create certificates",
        commands=["certbot -n --nginx --agree-tos -m zachgarwood@gmail.com", "certbot -n certificates"],
        sudo=True,
    )

infrastructure_files = Path(host.data.project_root) / "infrastructure" / "files"
files.link(
    name="Configure Joy Jar socket",
    path="/etc/systemd/system/joyjar.socket",
    target=str(infrastructure_files / "joyjar.socket"),
    symbolic=True,
    sudo=True,
)
init.systemd(
    name="Enable Joy Jar socket",
    service="joyjar.socket",
    enabled=True,
    running=False,
    sudo=True,
)

files.link(
    name="Configure Joy Jar service",
    path="/etc/systemd/system/joyjar.service",
    target=str(infrastructure_files / "joyjar.service"),
    symbolic=True,
    sudo=True,
)
init.systemd(
    name="Restart Joy Jar service",
    service="joyjar.service",
    enabled=True,
    restarted=True,
    running=True,
    sudo=True,
)

files.link(
    name="Configure nginx",
    path="/etc/nginx/sites-enabled/joyjar.conf",
    target=str(infrastructure_files / "nginx.conf"),
    present=True,
    sudo=True,
    symbolic=True,
)
server.shell(
    name="Check nginx configuration",
    commands="nginx -t",
    sudo=True,
)

init.systemd(
    name="Restart nginx service",
    service="nginx",
    enabled=True,
    restarted=True,
    running=True,
    sudo=True,
)
