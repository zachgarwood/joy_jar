from pathlib import Path

from pyinfra.operations import files, server


application = "joyjar"
group = "www-data"
for each_group in [application, group]:
    server.group(
        name=f"Ensure {each_group} group",
        present=True
    )

public_key = Path().home().joinpath(".ssh", f"{application}.pub")
server.user(
    name="Ensure application user",
    user=application,
    ensure_home=True,
    group=group,
    groups=[application, group],
    present=True,
    public_keys=[public_key.read_text()],
    shell="/bin/bash",
)

files.line(
    name="Ensure application user is a passwordless sudoer",
    path="/etc/sudoers",
    line=f"{application} .*",
    present=True,
    replace=f"{application} ALL=(ALL) NOPASSWD: ALL",
)
