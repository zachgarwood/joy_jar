from pathlib import Path

from pyinfra import host
from pyinfra.operations import files, server


# For more information: https://github.com/pyenv/pyenv-installer
if not host.fact.directory(host.data.pyenv_root):
    name = "pyenv-installer"
    commit_hash = "dd3f7d0914c5b4a416ca71ffabdf2954f2021596"
    path = Path("/tmp") / name
    file_name = commit_hash + ".sh"
    installer = path / file_name

    files.directory(
        name="Ensure pyenv download directory",
        path=str(path),
    )
    files.download(
        name="Download pyenv installer",
        src=f"https://raw.githubusercontent.com/pyenv/pyenv-installer/{commit_hash}/bin/{name}",
        dest=str(installer),
        mode="700",
    )
    server.shell(
        name="Install pyenv",
        commands=["rm -rf .pyenv", f". {installer}"],
        shell_executable="sh",
    )

files.put(
    name="Set pyenv initialization script",
    src="files/pyenv_init.sh",
    dest="/home/joyjar/.pyenv_init.sh",
)
server.script(
    name="Run pyenv initialization script",
    src="files/pyenv_init.sh",
)
files.line(
    name="Ensure pyenv initialization on shell startup",
    path="/home/joyjar/.bashrc",
    line=r".pyenv_init.sh",
    replace=". $HOME/.pyenv_init.sh",
    present=True,
    interpolate_variables=False,
)

server.shell(
    name="Check pyenv version",
    commands="pyenv --version",
)
