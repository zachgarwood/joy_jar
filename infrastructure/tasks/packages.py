from pyinfra.operations import apt, server


apt_ppas = ["universe", "ppa:certbot/certbot"]
for ppa in apt_ppas:
    apt.ppa(
        name=f"Ensure apt ppa {ppa}",
        src=ppa,
        present=True,
        sudo=True,
    )

apt.update(
    name="Update apt repositiories",
    touch_periodic=True,
    sudo=True,
)
certbot_packages = ["certbot", "python3-certbot-nginx", "software-properties-common"]
operational_packages = ["nginx", "sqlite3"]
pyenv_installer_packages = ["bash", "curl", "git"]
# https://github.com/pyenv/pyenv/wiki#suggested-build-environment
pyenv_packages = [
    "build-essential",
    "curl",
    "libbz2-dev",
    "libffi-dev",
    "liblzma-dev",
    "libncurses5-dev",
    "libreadline-dev",
    "libsqlite3-dev",
    "libssl-dev",
    "libxml2-dev",
    "libxmlsec1-dev",
    "llvm",
    "make",
    "python-openssl",
    "tk-dev",
    "wget",
    "xz-utils",
    "zlib1g-dev",
]
apt_packages = (
    certbot_packages + operational_packages + pyenv_installer_packages + pyenv_packages
)
apt.packages(
    name="Install apt packages",
    packages=apt_packages,
    cache_time=360,
    latest=True,
    present=True,
    sudo=True,
)
server.shell(
    name="Clean up after apt",
    commands="apt-get autoremove -y",
    sudo=True,
)
