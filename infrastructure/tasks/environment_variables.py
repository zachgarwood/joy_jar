from pyinfra.operations import files


files.put(
    name="Set environment variables",
    src="files/.environment.sh",
    dest="/home/joyjar/.environment.sh",
    assume_exists=False,
)
files.line(
    name="Ensure environment initialization on shell startup",
    path="/home/joyjar/.bashrc",
    line=r".environment.sh",
    replace=". $HOME/.environment.sh",
    present=True,
    interpolate_variables=False,
)

files.put(
    name="Set environment variable file",
    src="environment",
    dest="/home/joyjar/environment",
    add_deploy_dir=False,
    assume_exists=False,
)
