#!/bin/bash -x
# Deploy from CI server

apt-get update -y && apt-get install -y \
    bash \
    curl \
    git \
    openssh-client
eval $(ssh-agent -s)
chmod a-rwx,u=rw $SSH_PRIVATE_KEY  # chmod 700
ssh-add $SSH_PRIVATE_KEY
mkdir -p ~/.ssh
chmod -R a-rwx,u=rwx ~/.ssh  # chmod 600
ssh-keyscan joyjar.zachgarwood.com > ~/.ssh/known_hosts
chmod a=r,u=rw ~/.ssh/known_hosts  # chmod 644
pip install pyinfra
pyinfra --version
pyinfra infrastructure/inventory.py \
    -v \
    --key $SSH_PRIVATE_KEY \
    infrastructure/install.py \
    infrastructure/build.py \
    infrastructure/serve.py
