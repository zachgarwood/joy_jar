from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

from core.views import account, jar, joy, static as static_pages

admin_urls = [path("admin/", admin.site.urls)]

static_urls = [
    path("", static_pages.splash, name="splash"),
    path("whats-new/", static_pages.whats_new, name="whats-new")
]

account_urls = [
    path("accounts/", include("mailauth.urls")),
    path("accounts/signup/", account.Signup.as_view(), name="signup"),
]

jar_urls = [
    path("jar/", jar.random_joy, name="jar"),
    path("jar/empty/", jar.empty, name="empty-jar"),
]

joy_urls = [
    path("joy/", joy.ListJoys.as_view(), name="list-joys"),
    path("joy/create/", joy.CreateJoy.as_view(), name="create-joy"),
    path("joy/<int:pk>/", joy.DetailJoy.as_view(), name="detail-joy"),
    path("joy/<int:pk>/update/", joy.UpdateJoy.as_view(), name="update-joy"),
    path("joy/<int:pk>/delete/", joy.DeleteJoy.as_view(), name="delete-joy"),
]

all_urls = [
    admin_urls,
    static_urls,
    account_urls,
    jar_urls,
    joy_urls,
    staticfiles_urlpatterns(),
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
]
urlpatterns = [url for urls in all_urls for url in urls]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
    ]
