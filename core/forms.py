from django.forms import (
    BooleanField,
    CheckboxInput,
    FileInput,
    ModelForm,
    Textarea,
)
from mailauth.forms import EmailLoginForm

from core.models import Joy


class JoyForm(ModelForm):
    class Meta:
        model = Joy
        fields = ("description", "image", "remove_image")
        help_texts = {
            "description": "Describe what gave you joy",
            "image": "Show what gave you joy",
        }
        widgets = {
            "description": Textarea(
                attrs={
                    "class": "textarea",
                    "placeholder": "What gave you joy recently?",
                }
            ),
            "image": FileInput(attrs={"class": "file-input"}),
        }

    remove_image = BooleanField(
        initial=None,
        required=False,
        widget=CheckboxInput(attrs={"up-switch": ".file-upload"}),
    )

    def save(self):
        if self.cleaned_data.get("remove_image"):
            self.instance.image = None
        return super().save()


class EmailSignupForm(EmailLoginForm):
    email_template_name = "registration/confirm_email_email.txt"
    html_email_template_name = "registration/confirm_email_email.html"
    subject_template_name = "registration/confirm_email_subject.txt"
