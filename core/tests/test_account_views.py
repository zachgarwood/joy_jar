from django.shortcuts import reverse
import pytest

from core.views.account import DUPLICATE_SIGNUP_MESSAGE, NO_PASSWORD_MESSAGE


def test_signup_no_password_message(client, message_in_response):
    response = client.get(reverse("signup"))
    assert message_in_response(
        NO_PASSWORD_MESSAGE, response
    ), "There is a flash message with the 'no password' message"


def test_signup_duplicate_signup_message(client, message_in_response, user):
    response = client.post(
        reverse("signup"), dict(email=user.email), follow=True
    )
    assert message_in_response(
        DUPLICATE_SIGNUP_MESSAGE, response
    ), "There is a flash message with 'You're already signed up!'"


def test_signup_redirect_on_auth(auth_client):
    response = auth_client.get(reverse("signup"), follow=True)
    expected_redirect = reverse("whats-new"), 302
    assert (
        expected_redirect in response.redirect_chain
    ), "An authenticated client is redirected to the What's New page"

    auth_client.logout()
    unauth_client = auth_client
    response = unauth_client.get(reverse("signup"))
    assert (
        response.status_code == 200
    ), "A unauthenticated client is not redirected"


def test_signup_user_creation(client, django_user_model):
    users = django_user_model.objects.all()
    assert users.count() == 0

    client.post(reverse("signup"), dict(email="test@exmple.com"))
    assert users.count() == 1


@pytest.mark.django_db
def test_signup_confirmation_email(client, mailoutbox):
    assert len(mailoutbox) == 0

    client.post(reverse("signup"), dict(email="test@example.com"))
    assert len(mailoutbox) == 1

    email = mailoutbox[0]
    assert "confirm email link" in email.subject
    assert "confirm your email address" in email.body
