from pathlib import Path

from django.utils import timezone

from core.models import Joy


def test_set_audit_fields(user):
    joy = Joy(description="TEST DESCRIPTION", user=user)
    assert joy.created_at is None

    before_created = timezone.now()
    joy.set_audit_fields()
    assert joy.created_at is not None
    assert joy.created_at > before_created

    last_viewed_at = joy.viewed_at
    joy.set_audit_fields()
    assert joy.viewed_at != last_viewed_at
    assert joy.viewed_at > last_viewed_at


def test_create_image_thumbnail(image, settings, user):
    image_path = Path(image.filename)
    settings.MEDIA_ROOT = image_path.parent
    joy = Joy(description="TEST DESCRIPTION", image=image_path.name, user=user)
    joy.create_image_thumbnail()
    assert joy.thumbnail
    assert Path(joy.image.name).stem in joy.thumbnail.name, "Image file stem appears in thumbnail file name"
    joy.image = None
    joy.create_image_thumbnail()
    assert not joy.thumbnail
