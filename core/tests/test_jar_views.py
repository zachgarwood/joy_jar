from datetime import timedelta

from django.urls import reverse
import pytest

from core.models import Joy
from core.views.jar import view_timeout


def test_jar_redirect_when_empty(auth_client, user):
    joys = Joy.objects.filter(user=user)
    assert joys.count() == 0

    response = auth_client.get(reverse("jar"), follow=True)
    expected_redirect = reverse("empty-jar"), 302
    assert (
        expected_redirect in response.redirect_chain
    ), "When there are no joys, the jar redirects to the empty jar page"


def test_jar_does_not_display_joys_viewed_recently(
    auth_client, recently_viewed_joy
):
    last_viewed_at = recently_viewed_joy.viewed_at
    response = auth_client.get(reverse("jar"), follow=True)
    expected_redirect = reverse("empty-jar"), 302
    assert (
        expected_redirect in response.redirect_chain
    ), "Recently viewed Joy is not displayed"
    recently_viewed_joy.refresh_from_db()
    assert recently_viewed_joy.viewed_at == last_viewed_at


def test_jar_updates_joys_viewed_at(auth_client, viewable_joy):
    last_viewed_at = viewable_joy.viewed_at
    auth_client.get(reverse("jar"))
    viewable_joy.refresh_from_db()
    assert viewable_joy.viewed_at > last_viewed_at


@pytest.fixture
def viewable_joy(user):
    after_timeout_elapses = view_timeout() - timedelta(minutes=1)
    viewed_after = Joy.objects.create(
        user=user, description="Joy viewed after the timeout has elapsed"
    )
    Joy.objects.filter(id=viewed_after.id).update(
        viewed_at=after_timeout_elapses
    )
    viewed_after.refresh_from_db()
    yield viewed_after


@pytest.fixture
def recently_viewed_joy(user):
    before_timeout_elapses = view_timeout() + timedelta(minutes=1)
    viewed_before = Joy.objects.create(
        user=user, description="Joy viewed before the timeout has elapsed"
    )
    Joy.objects.filter(id=viewed_before.id).update(
        viewed_at=before_timeout_elapses
    )
    viewed_before.refresh_from_db()
    yield viewed_before
