import pytest
from PIL import Image


@pytest.fixture
def user(django_user_model, transactional_db):
    yield django_user_model.objects.create_user(email="test@example.com")


@pytest.fixture
def auth_client(client, user):
    client.force_login(user=user)
    yield client


@pytest.fixture
def message_in_response():
    def _message_in_response(message, response):
        if response.context is None or "messages" not in response.context:
            return False
        messages = list(response.context["messages"])
        return any(message in notification.message for notification in messages)

    return _message_in_response


@pytest.fixture
def image(tmp_path):
    image_path = tmp_path / "test.png"
    image = Image.new("1", (1, 1))
    image.save(image_path)
    with Image.open(image_path) as image:
        yield image
