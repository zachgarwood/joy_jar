from pathlib import Path

from core.forms import JoyForm
from core.models import Joy


def test_save(image, settings, user):
    image_path = Path(image.filename)
    settings.MEDIA_ROOT = image_path.parent
    joy_data = dict(description="TEST", image=image_path.name, user=user)
    joy = Joy(**joy_data)
    joy.save()
    assert joy.image and joy.thumbnail
    form_data = joy_data.copy()
    form_data["remove_image"] = True
    form = JoyForm(instance=joy, data=form_data)
    assert form.is_valid()
    joy = form.save()
    assert not (joy.image or joy.thumbnail)
