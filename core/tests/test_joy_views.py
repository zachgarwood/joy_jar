from urllib.parse import urljoin
from pathlib import Path

from django.urls import reverse

from core.models import Joy
from core.views.joy import (
    JOY_CREATE_MESSAGE,
    JOY_DELETE_MESSAGE,
    JOY_UPDATE_MESSAGE,
)


def test_joy_status_messages(auth_client, message_in_response):
    create_response = auth_client.post(
        reverse("create-joy"), dict(description="DESCRIPTION"), follow=True
    )
    assert message_in_response(
        JOY_CREATE_MESSAGE, create_response
    ), "There is a flash message with 'Joy added'"

    joy = create_response.context["object"]
    update_url = urljoin(joy.get_absolute_url(), "update/")
    update_response = auth_client.post(
        update_url, dict(description="UPDATED DESCRIPTION"), follow=True
    )
    assert message_in_response(
        JOY_UPDATE_MESSAGE, update_response
    ), "There is a flash message with 'Joy updated'"

    delete_url = urljoin(joy.get_absolute_url(), "delete/")
    delete_response = auth_client.delete(delete_url, follow=True)
    assert message_in_response(
        JOY_DELETE_MESSAGE, delete_response
    ), "There is a flash message with 'Joy removed'"


def test_removing_image(auth_client, image, settings, user):
    image_path = Path(image.filename)
    settings.MEDIA_ROOT = image_path.parent
    joy = Joy(description="TEST", image=image_path.name, user=user)
    joy.save()
    assert joy.image and joy.thumbnail
    auth_client.post(
        reverse("update-joy", args=(joy.id,)),
        dict(description="TEST", remove_image=True),
    )
    joy.refresh_from_db()
    assert not (joy.image or joy.thumbnail)
