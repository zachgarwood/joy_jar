from datetime import timedelta

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils import timezone

from core.models import Joy


def view_timeout():
    """Don't display a Joy for at least this long after viewing."""
    return timezone.now() - timedelta(minutes=5)


@login_required
def random_joy(request):
    """Display one random Joy that hasn't been viewed/created recently.
    If none fit that description, redirect to the empty jar page.
    """
    random_joy = (
        Joy.objects.filter(user=request.user, viewed_at__lt=view_timeout())
        .order_by("?")
        .first()
    )
    if not random_joy:
        return redirect("empty-jar")
    random_joy.save(update_fields=("viewed_at",))
    return render(request, "core/random_joy_detail.html", context=dict(object=random_joy))


@login_required
def empty(request):
    return render(request, "static/empty.html")
