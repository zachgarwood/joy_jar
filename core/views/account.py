from textwrap import dedent

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import UserPassesTestMixin
from django.db import IntegrityError
from django.shortcuts import redirect
from mailauth.views import LoginView as EmailLoginView

from core.forms import EmailSignupForm

DUPLICATE_SIGNUP_MESSAGE = "You're already signed up!"
NO_PASSWORD_MESSAGE = dedent(
    """
    Instead of creating yet another password to remember, whenever
    you log in, we'll email you a login link.

    To sign up, we just need to confirm your email address first.
    """
)


class Signup(UserPassesTestMixin, EmailLoginView):
    redirect_field_name = None
    form_class = EmailSignupForm

    def test_func(self):
        """Only show the signup form to non-authed users"""
        return not self.request.user.is_authenticated

    def handle_no_permission(self):
        """Redirect authed users to the What's New page"""
        return redirect("whats-new")

    def get(self, request, *args, **kwargs):
        messages.info(request, NO_PASSWORD_MESSAGE)
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        """Create the user before sending a login link email"""
        try:
            EmailUser = get_user_model()
            EmailUser.objects.create_user(
                form.cleaned_data[EmailUser.get_email_field_name()]
            )
        except IntegrityError as exception:
            if "UNIQUE constraint failed" in exception.args[0]:
                messages.warning(self.request, DUPLICATE_SIGNUP_MESSAGE)
            else:
                raise
        return super().form_valid(form)
