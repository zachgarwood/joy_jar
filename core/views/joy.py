from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from core.forms import JoyForm
from core.models import Joy

JOY_CREATE_MESSAGE = "Joy added"
JOY_DELETE_MESSAGE = "Joy removed"
JOY_UPDATE_MESSAGE = "Joy updated"


class CurrentUserFilterMixin:
    """Filter the view's queryset to the currently authed user"""

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class CreateJoy(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    form_class = JoyForm
    model = Joy
    success_message = JOY_CREATE_MESSAGE

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class UpdateJoy(
    CurrentUserFilterMixin, LoginRequiredMixin, SuccessMessageMixin, UpdateView
):
    form_class = JoyForm
    model = Joy
    success_message = JOY_UPDATE_MESSAGE


class DeleteJoy(CurrentUserFilterMixin, LoginRequiredMixin, DeleteView):
    model = Joy
    success_url = reverse_lazy("list-joys")

    def delete(self, request, *args, **kwargs):
        response = super().delete(request, *args, **kwargs)
        messages.info(request, JOY_DELETE_MESSAGE)
        return response


class DetailJoy(CurrentUserFilterMixin, LoginRequiredMixin, DetailView):
    model = Joy


class ListJoys(CurrentUserFilterMixin, LoginRequiredMixin, ListView):
    model = Joy
    ordering = ["-created_at"]
