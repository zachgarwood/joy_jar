from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render


@user_passes_test(
    lambda user: not user.is_authenticated, "whats-new", redirect_field_name=None
)
def splash(request):
    return render(request, "static/splash.html")


def whats_new(request):
    return render(request, "static/whats-new.html")
