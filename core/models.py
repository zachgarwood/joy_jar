from pathlib import Path

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import (
    CASCADE,
    DateTimeField,
    ForeignKey,
    ImageField,
    Model,
    TextField,
)
from django.urls import reverse
from django.utils import timezone
from PIL import Image


class Joy(Model):
    class Meta:
        ordering = ("-created_at",)

    description = TextField()
    image = ImageField(blank=True, null=True)
    thumbnail = ImageField(blank=True, null=True)
    created_at = DateTimeField(editable=False)
    viewed_at = DateTimeField()
    user = ForeignKey(get_user_model(), on_delete=CASCADE, related_name="joys")

    def get_absolute_url(self):
        return reverse("detail-joy", args=(self.pk,))

    def save(self, *args, **kwargs):
        self.set_audit_fields()
        self.create_image_thumbnail(**kwargs)
        super().save(*args, **kwargs)

    def set_audit_fields(self):
        now = timezone.now()
        if not self.id:
            self.created_at = now
        self.viewed_at = now

    def create_image_thumbnail(self, **kwargs):
        update_fields = kwargs.get("update_fields")
        if not (update_fields is None or "thumbnail" in update_fields):
            return
        if not self.image:
            self.thumbnail = None
            return
        stem = Path(self.image.name).stem
        if self.thumbnail and stem in self.thumbnail.name:
            return

        try:
            thumbnail_name = f"{stem}-128x128.png"
            thumbnail_path = Path(settings.MEDIA_ROOT) / thumbnail_name
            self.image.open()
            with Image.open(self.image) as image:
                image.thumbnail((128, 128))
                image.save(thumbnail_path)
            self.thumbnail.name = thumbnail_name
        except OSError:
            raise

    def __str__(self):
        return self.description
